package u03

import ExtendedLists._
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import u02.Optionals.Option.{None, Some}
import Lists.List.{Cons, Nil}

class ExtendedListsTest {

  val lst = Cons(10, Cons(20, Cons(30, Nil())))
  val fold = Cons(3, Cons(7, Cons(1, Cons(5, Nil()))))

  @Test def testDrop(): Unit = {
    assertEquals(Cons(20, Cons(30, Nil())), drop(lst, 1))
    assertEquals(Cons(30, Nil()), drop(lst, 2))
    assertEquals(Nil(), drop(lst, 5))
    assertEquals(Nil(), drop(Nil(), 5))
    assertEquals(Cons(10, Cons(20, Cons(30, Nil()))), drop(lst, 0))
  }

  @Test def testFlatMap(): Unit = {
    assertEquals(Cons(11, Cons(21, Cons(31, Nil()))), flatMap(lst)(v => Cons(v+1, Nil())))
    assertEquals(Cons(11, Cons(12, Cons(21, Cons(22, Cons(31, Cons(32, Nil())))))), flatMap(lst)(v => Cons(v+1, Cons(v+2, Nil()))))
  }

  @Test def testMap(): Unit = {
    assertEquals(Cons(11, Cons(21, Cons(31, Nil()))), map(lst)(v => v+1))
  }

  @Test def testFilter(): Unit = {
    assertEquals(Cons(10, Nil()), filter(lst)(_ < 20))
  }

  @Test def testMax(): Unit = {
    assertEquals(Some(25), max(Cons(20, Cons(25, Cons(15, Nil())))))
    assertEquals(None(), max(Nil()))
  }

  @Test def testFoldLeft(): Unit = {
    assertEquals(-16, foldLeft(fold)(0)(_-_))
  }

  @Test def testFoldRight(): Unit = {
    assertEquals(-8, foldRight(fold)(0)(_-_))
  }
}
