package u03

import ExtendedStreams._
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import Streams._
import Streams.Stream._
import Lists.List.{Cons, Nil}

class ExtendedStreamsTest {

  val s = Stream.take(Stream.iterate(0)(_+1))(10)
  val fibs = fibonacci()

  @Test def testDrop(): Unit = {
    assertEquals(Cons(6, Cons(7, Cons(8, Cons(9, Nil())))), toList(drop(s)(6)))
    assertEquals(toList(s), toList(drop(s)(0)))
    assertEquals(toList(empty()), toList(drop(empty())(5)))
  }

  @Test def testCostant(): Unit = {
    assertEquals(Cons("x", Cons("x", Cons("x", Cons("x", Cons("x", Nil()))))), toList(take(costant("x"))(5)))
  }

  @Test def testFibonacci(): Unit = {
    assertEquals(Cons(0, Cons(1, Cons(1, Cons(2, Cons(3, Cons(5, Cons(8, Nil()))))))), toList(take(fibs)(7)))
  }

}
