package u03

import Streams._
import Streams.Stream._

object ExtendedStreams {

  def drop[A](stream: Stream[A])(n: Int): Stream[A] = stream match {
    case Cons(_,t) if n > 0 => drop(t())(n-1)
    case _ => stream
  }

  def costant[A](k: A): Stream[A] = cons(k, costant(k))

  def fibonacci(): Stream[Int] = {
    def _fib(prev: Int, curr: Int): Stream[Int] = cons(prev, _fib(curr, prev+curr))
    _fib(0, 1)
  }

}
