package u03

import u02.Optionals.Option
import u02.Optionals.Option.{None, Some, getOrElse}
import Lists.List
import Lists.List.{Cons, Nil, append}
import u02.Modules._
import u02.Modules.Person._

import scala.annotation.tailrec

object ExtendedLists {

  def drop[A](l: List[A], n: Int): List[A] = l match {
    case Cons(_, t) if n > 0 => drop(t, n - 1)
    case _ => l
  }

  def flatMap[A, B](l: List[A])(f: A => List[B]): List[B] = l match {
    case Cons(h, t) => append(f(h), flatMap(t)(f))
    case _ => Nil()
  }

  def map[A, B](lst: List[A])(mapper: A => B): List[B] = flatMap(lst)(a => Cons(mapper(a), Nil()))

  def filter[A](lst: List[A])(pred: A => Boolean): List[A] = flatMap(lst) {
    case a if pred(a) => Cons(a, Nil())
    case _ => Nil()
  }

  def max(l: List[Int]): Option[Int] = l match {
    case Cons(h, t) => Some(Math.max(h, getOrElse(max(t), Int.MinValue)))
    case _ => None()
  }

  def getCourses(person: List[Person]): List[String] = flatMap(person){
    case Teacher(_, course) => Cons(course, Nil())
    case Student(_, _ ) => Nil()
  }

  @tailrec
  def foldLeft[A, B](list: List[A])(base: B)(op: (B, A) => B): B = list match {
    case Cons(h, t) => foldLeft(t)(op(base, h))(op)
    case _ => base
  }

  def foldRight[A, B](list: List[A])(base: B)(op: (A, B) => B): B = list match {
    case Cons(h,t) => op(h, foldRight(t)(base)(op))
    case Nil() => base
  }

}
